# Introdução

## Objetivo do módulo

Promover o fluxo de trabalho entre os participantes.


## Requisitos

- Conta no [Toggl](https://toggl.com)
- Conta no [GitLab](http://gitlab.com)
- Conta no [Slack](https://tangerina-solutions.slack.com/)
- Instalar o [Git](https://git-scm.com/downloads)
- Instalar o [Python3](https://www.python.org/downloads/)


### Conta no Toggl

Como dito dito a vocês, eu espero que vocês dediquem pelo menos 27 minutos por dia ao projeto. Porque 27? Eu sugiro a utilização da [técnica Pomodoro](https://pt.wikipedia.org/wiki/T%C3%A9cnica_pomodoro) para o foco no trabalho, e embora as medidas sejam pessoais, seguem as minhas: 27-7-17 (Pomodoro de 27 minutos, pausa de 7 minutos, Descanso de 17 minutos). A técnica consiste em focar em uma tarefa por determinada unidade de tempo, o pomodoro, evitando ao máximo as distrações que venham a ocorrer (Facebook, Twitter, WhatsApp, Globoesporte.com...). Ao término desta unidade de tempo, tire uns minutinhos (3-7) para olhar outra coisa, tomar uma água, ir ao banheiro (eu faço uma pausa de 7 minutos porque incluo aí alguma ação de controle pessoal da tarefa, como uma anotação no meu quadro branco, uma análise de algum dado, ou uma mensagem para os interessados, ajustar o nome/tempo da tarefa)... Para cada 4 pomodoros bem sucedidos (pomodoros interrompidos não contam), promova uma pausa no seu trabalho (levante da cadeira, dê uma movimentada, alongue-se, coma uma fruta, fale com alguém, etc).

O medidor de tempo que eu uso profissionalmente no momento é o Toggl, que você dá uma descrição do que está fazendo, aperta o play/stop e ele marca o tempo. A proposta é que você faça uma conta e me convide para que eu possa consultar seu progresso e ir ajustando nosso fluxo de trabalho. 
Utilize a ferramenta que você quiser para controlar, existem diversos aplicativos, desde desktop até celular e webapps disponíveis. Claro que você também pode controlar no papel e caneta =]
Lembrando que o pomodoro não precisa ser exato, às vezes vale a pena estender ou encurtar o pomodoro um pouquinho para não perder o raciocínio.

#### Criando um Workspace

Após logar no Toggl, clique no menu [Workspaces](https://toggl.com/app/workspaces). Um Workspace é uma separação lógica de contextos de trabalho, como "Tangerina" e "Projetos Pessoais".

#### Criando um cliente

No menu [Clients](https://toggl.com/app/workspaces/clients) você pode separar os agrupadores de tarefa dentro de um cliente do contexto, no nosso caso "tangerina.gitlab.io" "Projeto Adoção". 
#### Criando um projeto

Um [Projeto](https://toggl.com/app/projects) é utilizado para especificar em que se está trabalhando para um cliente, como "Manutenção" e "Conteúdo" da página, que nós vamos separar neste caso.

Lista de tarefas:
[ ] Criar conta no Toggl.
[ ] Criar o Workspace "Tangerina".
[ ] Criar os clientes "Estudos", "tangerina.gitlab.io" e "Projeto Adoção".
[ ] Criar os projetos "Manutenção" e "Conteúdo" no cliente "tangerina.gitlab.io".
[ ] Convidar o e-mail raphaelncampos@gmail.com como participante do Workspace.

### Conta no GitLab

O GitLab é uma estrutura que vamos utilizar para o ciclo de vida do desenvolvimento das nossas aplicações. Ele armazena nosso código-fonte, nossos bugs, e até a nossa página. Crie a sua conta, e me passe os dados de identificação para que eu lhe insira no [nosso grupo](https://gitlab.com/tangerina).

Lista de tarefas:
[ ] Criar a conta no GitLab.
[ ] Enviar o par [nome_de_usuario / email] para raphaelncampos@gmail.com para ser inserido no time.

### Conta no Slack

O Slack é uma ferramenta de comunicação para times que tem sido muito utilizada no mercado. Tem a vantagem de permitir comunicação privada, separação por canais, integração com bots e apps, e muito mais. Embora a gente tenha nosso [Grupo de Whatsapp](https://chat.whatsapp.com/JjS2z5kdiLwJpQGeWYmRjW) para uma comunicação mais informal e direta, vamos priorizar o slack  para a comunicação sobre os nosso projetos.
Ele é separado por workspaces, no formato <nome_do_workspace>.slack.com, que requer convite do administrador para entrar.

Lista de tarefas:

[ ] Solicitar o convite ao workspace por qualquer meio (Whatsapp, Telegram, email) ao administrador [Raphael Campos](raphaelncampos@gmail.com).

### Instalação do Git e do Python3

precisamos da instalação do Git para a nossa comunicação com o GitLab. Siga o procedimento de instalação para o seu sistema operacional. O Python3 será utilizado para rodar o projeto da nossa página, que foi feita com o [Pelican](https://blog.getpelican.com/)
